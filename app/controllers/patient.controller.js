const jwt = require("./../middlewares/jwt");
const epic = require("./../middlewares/epic");
const cerner = require("./../middlewares/cerner");

exports.addPatient = async (req, res) => {
  console.log("Patient add ---");
  // Get access token
  await jwt.getAccessToken("", async (resp) => {
    if (resp && resp.access_token) {
      const reqData = {
        accessToken: resp.access_token,
        data: req.body,
      };
      console.log("reqData--------", reqData);

      await epic.createPatient(reqData, async (response) => {
        console.log(response, "responce...");
        console.log(reqData.data, "reqdata...");
        if (response && response.status === 200) {
          res.status(200).send({
            status: 200,
            message: response.message,
            data: response.data,
          });
        } else {
          res.status(400).send({ status: 400, message: response.message });
        }
      });
    } else {
      res.status(400).send({ status: 400, message: "error" });
    }
  });
};
exports.addPatientCerner = async (req, res) => {
  console.log("Patient add ---");

  // Get access token
  await jwt.getAccessTokenCerner("", async (resp) => {
    console.log(resp, "resp...");
    if (resp && resp.access_token) {
      const reqData = {
        accessToken: resp.access_token,
        data: req.body,
      };
      console.log("reqData--------", reqData);

      await cerner.createPatient(reqData, async (response) => {
        console.log(response, "responce....");
        if (response && response.status === 200) {
          res.status(200).send({
            status: 200,
            message: response.message,
            data: response.data,
          });
        } else {
          res.status(400).send({ status: 400, message: response.message });
        }
      });
    } else {
      res.status(400).send({ status: 400, message: "error" });
    }
  });
};

// Find a single Patient with an id
exports.findOne = async (req, res) => {
  console.log("Patient FindOne ---", req.params);
  // Get access token
  await jwt.getAccessToken("", async (resp) => {
    if (resp && resp.access_token) {
      let result = {
        patient: {},
        practitioner: {},
        allergy: {},
        diagnosis: {},
        medicationOrder: {},
        medicationRequest: {},
        relatedPerson: {},
      };
      const reqData = {
        patientId: req.params.id,
        accessToken: resp.access_token,
      };
      console.log("--------", reqData);

      // Get Patient Records
      await epic.getPatientRecord(reqData, async (response) => {
        if (response) {
          result.patient = response;
          if (result.patient.issue) {
            res
              .status(200)
              .send({ status: 200, message: "error", data: result.patient });
          } else {
            // get Practitioner Record
            const reqDataPractitioner = {
              practitionerId: response.generalPractitioner[0].reference,
              accessToken: resp.access_token,
            };
            await epic.getPractitionerRecord(
              reqDataPractitioner,
              async (practitionerResponse) => {
                if (practitionerResponse) {
                  result.practitioner = practitionerResponse;
                }
              }
            );

            // get Patient Allergy Record
            await epic.getAllergyIntolerance(
              reqData,
              async (allergyResponse) => {
                if (allergyResponse) {
                  result.allergy = allergyResponse;
                }
              }
            );

            // get Patient Diagnosis Record
            await epic.getDiagnosticReport(
              reqData,
              async (diagnosisResponse) => {
                if (diagnosisResponse) {
                  result.diagnosis = diagnosisResponse;
                }
              }
            );

            // get Patient Medication Order
            await epic.getMedicationOrder(reqData, async (orderResponse) => {
              if (orderResponse) {
                result.medicationOrder = orderResponse;
              }
            });

            // get Patient Medication Request
            await epic.getMedicationRequest(
              reqData,
              async (requestResponse) => {
                if (requestResponse) {
                  result.medicationRequest = requestResponse;
                }
              }
            );

            // get Patient Related Person
            await epic.getRelatedPerson(reqData, async (relatedResponse) => {
              if (relatedResponse) {
                result.relatedPerson = relatedResponse;
              }
            });

            // Bindup data and send response
            setTimeout(function () {
              console.log("result---", result);
              res
                .status(200)
                .send({ status: 200, message: "success", data: result });
            }, 8000);
          }
        } else {
          res.status(400).send({ status: 400, message: "error" });
        }
      });
    } else {
      res.status(400).send({ status: 400, message: "error" });
    }
  });
};
exports.findOneCerner = async (req, res) => {
  console.log("Patient FindOne ---", req.params);
  // Get access token
  await jwt.getAccessTokenCerner("", async (resp) => {
    console.log(resp, "resp.....................");
    if (resp) {
      console.log("in side.", resp);
      let result = {
        patient: {},
        practitioner: {},
        allergy: {},
        diagnosis: {},
        medicationOrder: {},
        medicationRequest: {},
        relatedPerson: {},
      };
      const reqData = {
        patientId: req.params.id,
        accessToken: resp.access_token,
      };
      console.log("--------", reqData);

      // Get Patient Records
      await cerner.getPatientRecord(reqData, async (response) => {
        console.log(response, "responce");
        if (response) {
          result.patient = response;
          if (result.patient.issue) {
            res
              .status(200)
              .send({ status: 200, message: "error", data: result.patient });
          } else {
            // get Practitioner Record
            const reqDataPractitioner = {
              practitionerId:
                response.entry[0].resource.generalPractitioner[0].reference,
              accessToken: resp.access_token,
            };
            await cerner.getPractitionerRecord(
              reqDataPractitioner,
              async (practitionerResponse) => {
                if (practitionerResponse) {
                  result.practitioner = practitionerResponse;
                }
              }
            );
            // get Patient Allergy Record
            await cerner.getAllergyIntolerance(
              reqData,
              async (allergyResponse) => {
                if (allergyResponse) {
                  result.allergy = allergyResponse;
                }
              }
            );
            // // get Patient Diagnosis Record
            await cerner.getDiagnosticReport(
              reqData,
              async (diagnosisResponse) => {
                if (diagnosisResponse) {
                  result.diagnosis = diagnosisResponse;
                }
              }
            );
            // // get Patient Medication Order
            await cerner.getMedicationOrder(reqData, async (orderResponse) => {
              if (orderResponse) {
                result.medicationOrder = orderResponse;
              }
            });
            // get Patient Medication Request
            await cerner.getMedicationRequest(
              reqData,
              async (requestResponse) => {
                if (requestResponse) {
                  result.medicationRequest = requestResponse;
                }
              }
            );
            // // get Patient Related Person
            await cerner.getRelatedPerson(reqData, async (relatedResponse) => {
              if (relatedResponse) {
                result.relatedPerson = relatedResponse;
              }
            });
            // Bindup data and send response
            setTimeout(function () {
              console.log("result---", result);
              res
                .status(200)
                .send({ status: 200, message: "success", data: result });
            }, 8000);
          }
        } else {
          res.status(400).send({ status: 400, message: "error" });
        }
      });
    } else {
      res.status(400).send({ status: 400, message: "error" });
    }
  });
};
exports.updatePatientCerner = async (req, res) => {
  console.log("Patient FindOne ---", req.params);
  // Get access token
  await jwt.getAccessTokenCerner("", async (resp) => {
    console.log(resp, "resp.....................");
    if (resp) {
      console.log("in side.", resp);
      let result = {
        patient: {},
        practitioner: {},
        allergy: {},
        diagnosis: {},
        medicationOrder: {},
        medicationRequest: {},
        relatedPerson: {},
      };
      const reqData = {
        accessToken: resp.access_token,
        patientId: req.params.id,
        data: req.body,
      };
      console.log("--------", reqData);

      // Get Patient Records
      await cerner.getPatientRecord(reqData, async (response) => {
        if (response) {
          result.patient = response;
          if (result.patient.issue) {
            res
              .status(200)
              .send({ status: 200, message: "error", data: result.patient });
          } else {
            console.log(
              response.entry[0].resource.meta.versionId,
              ".............................................test.........................."
            );
            // get Practitioner Record
            reqData["versionId"] = response.entry[0].resource.meta.versionId;
            reqData["telecomId"] = response.entry[0].resource.telecom[0].id;
            reqData["emailId"] = response.entry[0].resource.telecom[1].id;
            await cerner.updatePatient(reqData, async (response) => {
              if (response && response.status === 200) {
                res.status(200).send({
                  status: 200,
                  message: response.message,
                  data: response.data,
                });
              } else {
                res
                  .status(400)
                  .send({ status: 400, message: response.message });
              }
            });
            // Bindup data and send response
            // setTimeout(function () {
            //   console.log("result---", result);
            //   res
            //     .status(200)
            //     .send({ status: 200, message: "success", data: result });
            // }, 8000);
          }
        } else {
          res.status(400).send({ status: 400, message: "error" });
        }
      });
    } else {
      res.status(400).send({ status: 400, message: "error" });
    }
  });
};

// Find a single Patient with an id
exports.search = async (req, res) => {
  console.log("Patient search ---", req.body);
  Object.entries(req.body).forEach(([key, value]) => {
    console.log(`${key}: ${value}`);
  });

  // const accessToken = await getAccessToken();
  // if (accessToken) {
  //   console.log('accessToken---', accessToken);
  // }

  // Get access token
  await jwt.getAccessToken("", async (resp) => {
    if (resp && resp.access_token) {
      const reqData = {
        accessToken: resp.access_token,
        family: "Mychart",
        birthdate: "1948-07-07",
      };
      console.log("--------", reqData);

      // Get Patient Records
      await epic.getPatientSearch(reqData, async (response) => {
        if (response) {
          res
            .status(200)
            .send({ status: 200, message: "success", data: response });
        } else {
          res.status(400).send({ status: 400, message: "error" });
        }
      });
    } else {
      res.status(400).send({ status: 400, message: "error" });
    }
  });
};

// Create new access token
const getAccessToken = () => {
  jwt.getAccessToken("", async (resp) => {
    if (resp && resp.access_token) {
      console.log("--------", resp.access_token);
    } else {
      res.status(400).send({ status: 400, message: "error" });
    }
  });
};
