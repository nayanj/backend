const request = require("request");
const qs = require("qs");

exports.getPatientRecord = async function (reqData, cb) {
  const url = `https://fhir-open.cerner.com/r4/ec2458f2-1e24-41c8-b71b-0e701af7583d/Patient?_id=${reqData.patientId}`;
  const header = {
    Authorization: `Bearer ${reqData.accessToken}`,
    Accept: "application/fhir+json",
  };

  request(
    {
      uri: url,
      method: "GET",
      headers: header,
      json: true,
    },
    function (err, res, body) {
      cb(body);
    }
  );
};
exports.createPatient = async function (reqData, cb) {
  const url = `https://fhir-ehr-code.cerner.com/r4/ec2458f2-1e24-41c8-b71b-0e701af7583d/Patient`;
  const header = {
    Authorization: `Bearer ${reqData.accessToken}`,
    Accept: "application/fhir+json",
    "Content-Type": "application/fhir+json",
  };
  const requestBody = JSON.stringify({
    resourceType: "Patient",
    extension: [
      {
        url: "http://hl7.org/fhir/us/core/StructureDefinition/us-core-birthsex",
        valueCode: "M",
      },
      {
        url: "http://hl7.org/fhir/us/core/StructureDefinition/us-core-race",
        extension: [
          {
            url: "ombCategory",
            valueCoding: {
              system: "urn:oid:2.16.840.1.113883.6.238",
              code: "2028-9",
              display: "Asian",
            },
          },
          {
            url: "detailed",
            valueCoding: {
              system: "urn:oid:2.16.840.1.113883.6.238",
              code: "2039-6",
              display: "Japanese",
            },
          },
        ],
      },
      {
        url: "http://hl7.org/fhir/us/core/StructureDefinition/us-core-ethnicity",
        extension: [
          {
            url: "ombCategory",
            valueCoding: {
              system: "urn:oid:2.16.840.1.113883.6.238",
              code: "2186-5",
              display: "Non Hispanic or Latino",
            },
          },
        ],
      },
    ],
    identifier: [
      {
        assigner: {
          reference: "Organization/675844",
        },
      },
    ],
    active: true,
    name: [
      {
        use: "official",
        family: "FHIR CERNER",
        given: ["Person", reqData.data.name],
        period: {
          start: "2010-05-17T14:54:31.000Z",
        },
      },
      {
        use: "usual",
        given: ["Bigby"],
        period: {
          start: "2012-05-22T15:45:50.000Z",
        },
      },
    ],
    telecom: [
      {
        system: "phone",
        value: reqData.data.phone,
        use: "home",

        extension: [
          {
            valueString: "12345",
            url: "http://hl7.org/fhir/StructureDefinition/contactpoint-extension",
          },
        ],
        period: {
          start: "2012-05-17T15:33:18.000Z",
        },
      },
      {
        system: "email",
        value: reqData.data.email,
        use: "home",

        extension: [
          {
            valueString: "12345",
            url: "http://hl7.org/fhir/StructureDefinition/contactpoint-extension",
          },
        ],
        period: {
          start: "2012-05-17T15:33:18.000Z",
        },
      },
    ],
    gender: reqData.data.gender,
    birthDate: reqData.data.birthDate,
    address: [
      {
        use: "home",
        line: [reqData.data.line, "Apartment 403"],
        city: reqData.data.city,
        district: "district",
        state: reqData.data.state,
        postalCode: reqData.data.postalCode,
        country: reqData.data.country,
        period: {
          start: "2012-05-17T15:33:18.000Z",
        },
      },
    ],
    maritalStatus: {
      coding: [
        {
          system: "http://terminology.hl7.org/CodeSystem/v3-NullFlavor",
          code: "UNK",
          display: reqData.data.maritalStatus,
        },
      ],
      text: reqData.data.maritalStatus,
    },
    communication: [
      {
        language: {
          coding: [
            {
              system: "urn:ietf:bcp:47",
              code: "en",
              display: "English",
            },
          ],
          text: "English",
        },
        preferred: true,
      },
    ],
    generalPractitioner: [
      {
        reference: "Practitioner/4122622",
      },
    ],
  });

  request(
    {
      uri: url,
      method: "POST",
      headers: header,
      body: requestBody,
    },
    function (err, res, body) {
      let finalResponse = {
        status: 400,
        message: "Something went wrong.",
        data: "",
      };

      if (body) {
        const bodyParse = JSON.parse(body);
        if (bodyParse.issue) {
          finalResponse.status = 400;
          finalResponse.message = bodyParse.issue[0].details.text;
        }
      } else {
        if (res.headers.location) {
          finalResponse.status = 200;
          finalResponse.message = "success";
          finalResponse.data = res.headers.location;
        }
      }
      cb(finalResponse);
    }
  );
};
exports.updatePatient = async function (reqData, cb) {
  const url = `https://fhir-ehr-code.cerner.com/r4/ec2458f2-1e24-41c8-b71b-0e701af7583d/Patient/${reqData.patientId}`;
  let version = '"' + reqData.versionId + '"';
  const header = {
    Authorization: `Bearer ${reqData.accessToken}`,
    Accept: "application/fhir+json",
    "Content-Type": "application/json-patch+json",
    "If-Match": `W/${version}`,
  };

  const requestBody = JSON.stringify([
    {
      path: "/name/0/id",
      op: "test",
      value: `CI-${reqData.patientId}-0`,
    },
    {
      path: "/gender",
      op: "replace",
      value: reqData.data.gender,
    },
    {
      path: "/birthDate",
      op: "replace",
      value: reqData.data.birthDate,
    },
    {
      path: "/telecom/0/id",
      op: "test",
      value: reqData.telecomId,
    },
    {
      path: "/telecom/0/value",
      op: "replace",
      value: reqData.data.phone,
    },
    {
      path: "/telecom/1/id",
      op: "test",
      value: reqData.emailId,
    },
    {
      path: "/telecom/1/value",
      op: "replace",
      value: reqData.data.email,
    },
    {
      path: "/name/0/given",
      op: "replace",
      value: ["Joe", reqData.data.name],
    },
  ]);

  request(
    {
      uri: url,
      method: "PATCH",
      headers: header,
      body: requestBody,
    },
    function (err, res, body) {
      let finalResponse = {
        status: 200,
        message: "patient updated successfully!",
        data: "",
      };

      if (body) {
        const bodyParse = JSON.parse(body);
        if (bodyParse.issue) {
          finalResponse.status = 400;
          finalResponse.message = bodyParse.issue[0].details.text;
        }
      } else {
        if (res.headers.location) {
          finalResponse.status = 200;
          finalResponse.message = "success";
          finalResponse.data = res.headers.location;
        }
      }
      cb(finalResponse);
    }
  );
};
exports.getDiagnosticReport = async function (reqData, cb) {
  const url = `https://fhir-open.cerner.com/dstu2/ec2458f2-1e24-41c8-b71b-0e701af7583d/DiagnosticReport?patient=${reqData.patientId}`;

  const header = {
    Authorization: `Bearer ${reqData.accessToken}`,
    Accept: "application/json+fhir",
  };

  request(
    {
      uri: url,
      method: "GET",
      headers: header,
      json: true,
    },
    function (err, res, body) {
      cb(body);
    }
  );
};
exports.getMedicationOrder = async function (reqData, cb) {
  const url = `https://fhir-open.cerner.com/dstu2/ec2458f2-1e24-41c8-b71b-0e701af7583d/MedicationOrder?patient=${reqData.patientId}`;
  const header = {
    Authorization: `Bearer ${reqData.accessToken}`,
    Accept: "application/json+fhir",
  };

  request(
    {
      uri: url,
      method: "GET",
      headers: header,
      json: true,
    },
    function (err, res, body) {
      cb(body);
    }
  );
};
exports.getRelatedPerson = async function (reqData, cb) {
  const url = `https://fhir-open.cerner.com/r4/ec2458f2-1e24-41c8-b71b-0e701af7583d/RelatedPerson?patient=${reqData.patientId}`;
  const header = {
    Authorization: `Bearer ${reqData.accessToken}`,
    Accept: "application/json+fhir",
  };

  request(
    {
      uri: url,
      method: "GET",
      headers: header,
      json: true,
    },
    function (err, res, body) {
      cb(body);
    }
  );
};
exports.getMedicationRequest = async function (reqData, cb) {
  const url = `https://fhir-open.cerner.com/r4/ec2458f2-1e24-41c8-b71b-0e701af7583d/MedicationRequest?patient=${reqData.patientId}`;
  const header = {
    Authorization: `Bearer ${reqData.accessToken}`,
    Accept: "application/json+fhir",
  };

  request(
    {
      uri: url,
      method: "GET",
      headers: header,
      json: true,
    },
    function (err, res, body) {
      cb(body);
    }
  );
};

exports.getAllergyIntolerance = async function (reqData, cb) {
  const url = `https://fhir-open.cerner.com/r4/ec2458f2-1e24-41c8-b71b-0e701af7583d/AllergyIntolerance?patient=${reqData.patientId}`;
  const header = {
    Authorization: `Bearer ${reqData.accessToken}`,
    Accept: "application/json+fhir",
  };

  request(
    {
      uri: url,
      method: "GET",
      headers: header,
      json: true,
    },
    function (err, res, body) {
      cb(body);
    }
  );
};
exports.getPractitionerRecord = async function (reqData, cb) {
  const url = `https://fhir-open.cerner.com/r4/ec2458f2-1e24-41c8-b71b-0e701af7583d/${reqData.practitionerId}`;
  const header = {
    Authorization: `Bearer ${reqData.accessToken}`,
    Accept: "application/json+fhir",
  };

  request(
    {
      uri: url,
      method: "GET",
      headers: header,
      json: true,
    },
    function (err, res, body) {
      cb(body);
    }
  );
};
