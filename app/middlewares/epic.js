const request = require("request");
const qs = require("qs");

exports.createPatient = async function (reqData, cb) {
  const url = `${process.env.EPIC_API_URL}/api/FHIR/R4/Patient`;
  const header = {
    Authorization: `Bearer ${reqData.accessToken}`,
    "Content-Type": "application/json",
  };
  const requestBody = JSON.stringify({
    resourcetype: "patient",
    identifier: [
      {
        use: "usual",
        system: "urn:oid:2.16.840.1.113883.4.1",
        value: "000-00-0019",
      },
    ],
    active: "true",
    name: [
      {
        use: "usual",
        family: "fhirpoc",
        given: [reqData.data.name],
      },
    ],
    telecom: [
      {
        system: "phone",
        value: reqData.data.phone,
        use: "home",
      },
      {
        system: "email",
        value: reqData.data.email,
      },
    ],
    gender: reqData.data.gender,
    birthDate: reqData.data.birthDate,
    address: [
      {
        use: "home",
        line: [reqData.data.line],
        city: reqData.data.city,
        state: reqData.data.state,
        postalCode: reqData.data.postalCode,
        country: reqData.data.country,
      },
    ],
    maritalStatus: {
      text: reqData.data.maritalStatus,
    },
    generalPractitioner: [
      {
        display: "Physician Family Medicine, MD",
        reference: "eM5CWtq15N0WJeuCet5bJlQ3",
      },
    ],
    extension: [
      {
        url: "http://open.epic.com/FHIR/STU3/StructureDefinition/patient-preferred-provider-language",
        valueCodeableConcept: {
          coding: [
            {
              system: "urn:oid:2.16.840.1.113883.6.99",
              code: "in",
            },
          ],
          text: "India",
        },
      },
      {
        url: "http://open.epic.com/FHIR/STU3/StructureDefinition/patient-preferred-provider-sex",
        valueCode: reqData.data.gender,
      },
    ],
  });

  request(
    {
      uri: url,
      method: "POST",
      headers: header,
      body: requestBody,
    },
    function (err, res, body) {
      let finalResponse = {
        status: 400,
        message: "Something went wrong.",
        data: "",
      };
      if (body) {
        const bodyParse = JSON.parse(body);
        if (bodyParse.issue) {
          finalResponse.status = 400;
          finalResponse.message = bodyParse.issue[0].details.text;
        }
      } else {
        if (res.headers.location) {
          finalResponse.status = 200;
          finalResponse.message = "success";
          finalResponse.data = res.headers.location;
        }
      }
      cb(finalResponse);
    }
  );
};

exports.getPatientRecord = async function (reqData, cb) {
  const url = `${process.env.EPIC_API_URL}/api/FHIR/R4/Patient/${reqData.patientId}`;
  const header = {
    Authorization: `Bearer ${reqData.accessToken}`,
    "Content-Type": "application/x-www-form-urlencoded",
  };

  request(
    {
      uri: url,
      method: "GET",
      headers: header,
      json: true,
    },
    function (err, res, body) {
      cb(body);
    }
  );
};

exports.getPractitionerRecord = async function (reqData, cb) {
  const url = `${process.env.EPIC_API_URL}/api/FHIR/R4/${reqData.practitionerId}`;
  const header = {
    Authorization: `Bearer ${reqData.accessToken}`,
    "Content-Type": "application/x-www-form-urlencoded",
  };

  request(
    {
      uri: url,
      method: "GET",
      headers: header,
      json: true,
    },
    function (err, res, body) {
      cb(body);
    }
  );
};

exports.getAllergyIntolerance = async function (reqData, cb) {
  const url = `${process.env.EPIC_API_URL}/api/FHIR/R4/AllergyIntolerance?patient=${reqData.patientId}`;
  const header = {
    Authorization: `Bearer ${reqData.accessToken}`,
    "Content-Type": "application/x-www-form-urlencoded",
  };

  request(
    {
      uri: url,
      method: "GET",
      headers: header,
      json: true,
    },
    function (err, res, body) {
      cb(body);
    }
  );
};

exports.getDiagnosticReport = async function (reqData, cb) {
  const url = `${process.env.EPIC_API_URL}/api/FHIR/R4/DiagnosticReport?patient=${reqData.patientId}`;
  const header = {
    Authorization: `Bearer ${reqData.accessToken}`,
    "Content-Type": "application/x-www-form-urlencoded",
  };

  request(
    {
      uri: url,
      method: "GET",
      headers: header,
      json: true,
    },
    function (err, res, body) {
      cb(body);
    }
  );
};

exports.getMedicationOrder = async function (reqData, cb) {
  const url = `${process.env.EPIC_API_URL}/api/FHIR/DSTU2/MedicationOrder?patient=${reqData.patientId}`;
  const header = {
    Authorization: `Bearer ${reqData.accessToken}`,
    "Content-Type": "application/x-www-form-urlencoded",
  };

  request(
    {
      uri: url,
      method: "GET",
      headers: header,
      json: true,
    },
    function (err, res, body) {
      cb(body);
    }
  );
};

exports.getMedicationRequest = async function (reqData, cb) {
  const url = `${process.env.EPIC_API_URL}/api/FHIR/R4/MedicationRequest?patient=${reqData.patientId}`;
  const header = {
    Authorization: `Bearer ${reqData.accessToken}`,
    "Content-Type": "application/x-www-form-urlencoded",
  };

  request(
    {
      uri: url,
      method: "GET",
      headers: header,
      json: true,
    },
    function (err, res, body) {
      cb(body);
    }
  );
};

exports.getRelatedPerson = async function (reqData, cb) {
  const url = `${process.env.EPIC_API_URL}/api/FHIR/R4/RelatedPerson/${reqData.patientId}`;
  const header = {
    Authorization: `Bearer ${reqData.accessToken}`,
    "Content-Type": "application/x-www-form-urlencoded",
  };

  request(
    {
      uri: url,
      method: "GET",
      headers: header,
      json: true,
    },
    function (err, res, body) {
      cb(body);
    }
  );
};

exports.getPatientSearch = async function (reqData, cb) {
  const accessToken = reqData.accessToken;
  delete reqData.accessToken;
  let query = qs.stringify(reqData);

  const url = `${process.env.EPIC_API_URL}/api/FHIR/R4/Patient?${query}`;
  const header = {
    Authorization: `Bearer ${accessToken}`,
    "Content-Type": "application/x-www-form-urlencoded",
  };

  request(
    {
      uri: url,
      method: "GET",
      headers: header,
      json: true,
    },
    function (err, res, body) {
      cb(body);
    }
  );
};
