const fs = require("fs");
const qs = require("qs");
const moment = require("moment");
const request = require("request");
const jwt = require("jsonwebtoken");

exports.getAccessToken = async function (reqData, cb) {
  const startTime = moment().unix();
  const endTime = startTime + 3; // added in seconds
  const params = {
    iss: process.env.EPIC_CLIENT_ID,
    sub: process.env.EPIC_CLIENT_ID,
    aud: `${process.env.EPIC_API_URL}/oauth2/token`,
    jti: "ProjectStartOn-20210915",
    exp: endTime,
    nbf: startTime,
    iat: startTime,
  };

  const privateKey = fs.readFileSync("./app/utils/privatekey.pem", "utf8");
  const token = jwt.sign(params, privateKey, { algorithm: "RS384" });
  const accessToken = await getBearerToken(token);

  cb(JSON.parse(accessToken));
};
exports.getAccessTokenCerner = async function (reqData, cb) {
  const startTime = moment().unix();
  const endTime = startTime + 3; // added in seconds
  const params = {
    iss: process.env.EPIC_CLIENT_ID,
    sub: process.env.EPIC_CLIENT_ID,
    aud: `${process.env.EPIC_API_URL}/oauth2/token`,
    jti: "ProjectStartOn-20210915",
    exp: endTime,
    nbf: startTime,
    iat: startTime,
  };

  const accessToken = await getBearerTokenForCerner();

  cb(JSON.parse(accessToken));
};

const getBearerToken = (jwtToken) => {
  return new Promise((resolve) => {
    const url = `${process.env.EPIC_API_URL}/oauth2/token`;
    const requestBody = qs.stringify({
      grant_type: "client_credentials",
      client_assertion_type:
        "urn:ietf:params:oauth:client-assertion-type:jwt-bearer",
      client_assertion: jwtToken,
    });
    const header = {
      "Content-Length": requestBody.length,
      "Content-Type": "application/x-www-form-urlencoded",
    };

    request(
      {
        uri: url,
        method: "POST",
        headers: header,
        body: requestBody,
      },
      function (err, res, body) {
        resolve(body);
      }
    );
  });
};
const getBearerTokenForCerner = () => {
  return new Promise((resolve) => {
    const url = `https://authorization.cerner.com/tenants/ec2458f2-1e24-41c8-b71b-0e701af7583d/protocols/oauth2/profiles/smart-v1/token`;
    const requestBody = qs.stringify({
      grant_type: "client_credentials",
      scope:
        "system/Patient.write system/Observation.read system/Observation.write system/Patient.read ",
    });
    const header = {
      Authorization:
        "Basic ZDUxZjNjYzEtMzBlOC00YjFjLWE3NjYtYzRmNTk2NGM4YTY5OmFPUnV0SnVKYkNlMjJkSUFseW9nc0EyUjFnM1ZjcEpk",
      Host: "authorization.cerner.com",
      Accept: "application/json",
      "Content-Type": "application/x-www-form-urlencoded",
      Connection: "close",
    };
    request(
      {
        uri: url,
        method: "POST",
        headers: header,
        body: requestBody,
      },
      function (err, res, body) {
        resolve(body);
      }
    );
  });
};
