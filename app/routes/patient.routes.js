module.exports = (app) => {
  const patients = require("../controllers/patient.controller.js");

  var router = require("express").Router();

  // Retrieve a single Patients with id
  router.get("/:id", patients.findOne);
  router.get("/cerner/:id", patients.findOneCerner);
  router.post("/cerner", patients.addPatientCerner);
  router.patch("/cerner/update/:id", patients.updatePatientCerner);

  router.post("", patients.addPatient);

  router.post("/search", patients.search);

  app.use("/api/patients", router);
};
